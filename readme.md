# Quiz-ME

A live test taking app built using MEAN stack technologies incuding angular.js Node, Mongodb and Expressjs.

## Features of this app:

### User side-

* A view to register on the system. Users can register through gmail or by creating an account on the app it self.
* A view to see all the avialable test on the system.
* A view to take the tests.
* A view to see the results of past tests as well as see the chart of average past performance,

### Admin side

* A view to create tests.
* A view to edit tests.
* A view to add questions to the test.
* A view to create questions.
* A view to edit questions.
* A view to see the test results of each student registered on the app including chart of their average performance.

### General

* This app uses material design for minimal yet intuitive ui experience.

## Installation:

1.  clone this repository

```bash
git clone https://github.com/codemayank/Quiz-me
```

2.  Navigate to the directory where the sourcecode for this app has been downloaded.

3.  Install all the dependencies by running

```bash
npm install
```

4.  Build the client side scripts

```bash
npm run build-dev
```

5.  Start the MongoDB server by running

```bash
./mongod
```

6.  Create a config.js file in .server/config folder and paste the below code.

```javascript
let env = process.env.NODE_ENV || "development";

// process.env.googleClientID = please enter your google client id here. the app will not run if googleClient Id is not set
// process.env.googleClientSecret = please enter your google client secret here the app will not run if google client secret is not set.
process.env.cookieKey = "my_cookie_secret"; //please change the key.
if (env === "development") {
  process.env.PORT = 3000;
  process.env.MONGODB_URI = "mongodb://localhost/quiz_me";
}
//uncomment the below code and enter the required credentialis to use the email feature of this app
// module.exports = {
//     emailAuth : {
//         useEmail : false //set to true if you want the app to send emails. To use the forgot password feature.
//         service : //enter the name of email service
//         auth : {
//             user : ,//please enter your email username
//             pass : ,//please enter your email password
//         }
//     }
// }
```

7.  Start the app by running.

```bash
node server/server.js
```

9.  The app should now be running on your local host on port 3000.

## Created by:

### Mayank Yadav
