class TestResultController {
  constructor(TestService, $routeParams) {
    "ngInject";

    this.TestService = TestService;
    this.$routeParams = $routeParams;
  }

  $onInit() {
    console.log("test result page loaded", this.$routeParams.test_id);
    this.TestService.getTestResult(this.$routeParams.test_id)
      .then(data => {
        console.log(data);
        this.test = data.test;
      })
      .catch(e => {
        console.log("error retreiving tests", e);
      });
  }
}

export default TestResultController;
