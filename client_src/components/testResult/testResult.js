import angular from "angular";

import testResultComponent from "./testResult.component";
const testResult = angular
  .module("testResult", [])
  .component("testResult", testResultComponent);
export default testResult;
