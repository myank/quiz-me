import controller from "./testResult.controller";
import template from "./testResult.html";

let testResultComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default testResultComponent;
