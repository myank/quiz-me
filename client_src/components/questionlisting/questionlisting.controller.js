class QuestionListingController {
  constructor(TestService) {
    this.TestService = TestService;
    this.edit = "edit";
    this.showNew = "showNewForm";
    this.selectedQuestions = [];
    $(".modal").modal();
  }

  $onInit() {
    this.questionListUrl = "/admin/question";
    this.TestService.getQuestionList(this.questionListUrl)
      .then(data => {
        this.questionList = data.questions;
        console.log(data);
      })
      .catch(e => {
        console.log("error fetching question list", e);
      });
  }

  editSelect(question) {
    this.editSelectq = question;
    console.log(this.editSelectq);
  }

  deleteQuestion(questionId) {
    this.TestService.deleteQuestion(questionId)
      .then(data => {
        console.log("logging data in delete question question manager", data);
        let index = this.questionList.findIndex(
          x => x._id === data.question._id
        );
        if (index > -1) {
          this.questionList.splice(index, 1);
          console.log(this.questionList);
        }
      })
      .catch(e => {
        console.log("error in deleting the question", e);
      });
  }
  updateQuestion(question, action) {
    console.log(question);
    if (action === "new") {
      this.questionList.push(question);
    } else if (action === "edit") {
      let index = this.questionList.findIndex(x => x._id === question._id);
      console.log(index);
      if (index > -1) {
        this.questionList[index] = question;
        console.log(this.questionList);
      }
    }
  }
}

export default QuestionListingController;
