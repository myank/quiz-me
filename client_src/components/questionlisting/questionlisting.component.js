import template from "./questionlisting.html";
import controller from "./questionlisting.controller";

let questionListingComponent = {
  bindings: {
    test: "<",
    onUpdate: "&"
  },
  template,
  controller,
  controllerAs: "vm"
};
export default questionListingComponent;
