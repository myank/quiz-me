import angular from "angular";

import questionListingComponent from "./questionlisting.component";
import questionManagerComponent from "../questionManager/questionmanager";

const questionListingModule = angular
  .module("questionListing", [questionManagerComponent.name])
  .component("questionListing", questionListingComponent);

export default questionListingModule;
