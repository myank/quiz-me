import template from "./signIn.html";
import controller from "./signIn.controller";

let signInComponent = {
  bindings: {
    userType: "<",
    action: "<"
  },
  template,
  controller,
  controllerAs: "vm"
};

export default signInComponent;
