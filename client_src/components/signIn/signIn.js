import angular from "angular";
import signInComponent from "./signIn.component";
const signInModule = angular
  .module("signIn", [])
  .component("signIn", signInComponent);
export default signInModule;
