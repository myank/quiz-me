class SignInController {
  constructor(UserService, $location, $timeout) {
    "ngInject";
    this.UserService = UserService;
    this.$location = $location;
    this.showResetEmailNotSent = true;
    this.$timeout = $timeout;
  }

  $onDestroy() {
    this.registrationForm = {};
    this.showResetPasswordForm = false;
    this.showResetEmailNotSent = true;
  }
  register() {
    this.UserService.register(this.registrationForm)

      .then(data => {
        if (data.admin_id) {
          this.$location.path("/admin/dashboard");
        } else {
          this.$location.path("/user/dashboard");
        }
        this.regitrationForm = {};
        this.showEmailError = false;
        this.ShowUserNameError = false;
      })
      .catch(e => {
        let re1 = /username_1/g;
        let re2 = /email_1/g;

        if (e.data.errmsg && re1.test(e.data.errmsg)) {
          this.showUserNameError = true;
          this.$timeout(() => {
            this.showUserNameError = false;
          }, 5000);
        }
        if (e.data.errmsg && re2.test(e.data.errmsg)) {
          this.showEmailError = true;
          this.$timeout(() => {
            this.showEmailError = false;
          }, 5000);
        }
      });
  }

  login() {
    this.UserService.login(this.registrationForm)
      .then(data => {
        if (data.admin_id) {
          this.$location.path("/admin/dashboard");
        } else {
          this.$location.path("/user/dashboard");
        }

        this.registrationForm = {};
      })
      .catch(e => {
        console.log("error", e);
        if (e.status === 401) {
          this.showWrongCredentials = "incorrect email address or password";
          this.$timeout(() => {
            this.showWrongCredentials = null;
          }, 5000);
        }
      });
  }
  googleAuth() {
    this.UserService.getGoogleAuth();
  }
  forgotPassword(userType) {
    let credentials = {
      email: this.forgotPasswordEmail,
      userType: userType
    };
    this.UserService.forgotPassword(credentials)
      .then(data => {
        this.showResetEmailNotSent = false;
        this.forgotPasswordEmail = "";
      })
      .catch(e => {
        console.log("error in sending the forgot password e-mail", e);
      });
  }
}
export default SignInController;
