import angular from "angular";
import testListingComponent from "./testlisting.component";

const testListingModule = angular
  .module("testListing", [])
  .component("testListing", testListingComponent);

export default testListingModule;
