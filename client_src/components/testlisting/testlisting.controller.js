class TestListingController {
  constructor(TestService, $location, moment) {
    "ngInject";

    //this will keep the service instance across our class.
    this.TestService = TestService;
    this.$location = $location;
    $(".modal").modal();
    $(".tooltipped").tooltip();
    this.moment = moment;

    this.testList = [];
  }

  //this method will be called each time the component will be initialised
  //In this case this will be called for every page route change.

  $onInit() {
    this.TestService.getTests()
      .then(data => {
        console.log("Test Data", data);
        this.testList = data.tests;
        console.log(this.testList);
        this.testList.forEach(test => {
          test.testTimeMinutes = Math.floor(test.timelimit / 60);
          test.testTimeSeconds = test.timelimit % 60;
        });
      })
      .catch(e => {
        console.log("error fetching tests", e);
      });
  }

  selectTest(test) {
    this.selectedTest = test;
  }
  goToTest() {
    this.$location.path("/test/" + this.selectedTest._id);
  }
}

export default TestListingController;
