import template from "./testlisting.html";
import controller from "./testlisting.controller";

let testListingComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default testListingComponent;
