import template from "./testlistingadmin.html";
import controller from "./testlistingadmin.controller";

let testListingAdminComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default testListingAdminComponent;
