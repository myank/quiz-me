import angular from "angular";
import testListingAdminComponent from "./testlistingadmin.component";

import testManagerComponent from "../testManager/testManager";
import questionManagerComponent from "../questionManager/questionmanager";

import questionListComponent from "../questionlisting/questionlisting";

const testListingAdminModule = angular
  .module("testListingAdmin", [
    testManagerComponent.name,
    questionManagerComponent.name,

    questionListComponent.name
  ])
  .component("testListingAdmin", testListingAdminComponent);
export default testListingAdminModule;
