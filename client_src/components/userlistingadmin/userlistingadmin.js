import angular from "angular";
import userListingAdminComponent from "./userlistingadmin.component";

const userListingAdminModule = angular
  .module("userListingAdmin", [])
  .component("userListingAdmin", userListingAdminComponent);
export default userListingAdminModule;
