import template from "./userlistingadmin.html";
import controller from "./userlistingadmin.controller";

let userListingAdminComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default userListingAdminComponent;
