class UserListingAdminController {
  constructor(UserService, $scope, moment) {
    this.UserService = UserService;
    this.$scope = $scope;
    this.$scope.labels = [];
    this.moment = moment;

    $(".modal").modal();
  }

  $onInit() {
    this.UserService.getUserList()
      .then(data => {
        this.userList = data;
        console.log("logging user list in userlisting admin controller", data);
        this.userList.forEach(user => {
          user.averagesArray = this.UserService.calculateAverages(user.tests);
          user.lastAveragePercentage = user.averagesArray.pop();
        });
      })
      .catch(e => {
        console.log("some error fetching userlist for admin", e);
      });
  }

  selectUser(user) {
    let counter = 0;
    this.$scope.labels = [];
    let AveragesArray = this.UserService.calculateAverages(user.tests);
    this.$scope.data = AveragesArray;
    user.tests.forEach(test => {
      if (counter === 0);
      test.attemptedDate = this.moment(test.attemptDate).format("Do MMM YY");
      this.$scope.labels.push(test.attemptedDate);
    });
    this.selectedUser = user;
  }
}

export default UserListingAdminController;
