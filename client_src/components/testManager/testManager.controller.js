class TestManagerController {
  constructor(TestService, $location) {
    this.TestService = TestService;
    this.$location = $location;

    this.mode = "new";
  }

  $onInit() {
    if (this.test) {
      this.testForm = this.test;
    }
  }

  $onChanges() {
    if (this.test) {
      this.testForm = this.test;
      this.mode = "edit";
      console.log(this.testForm);
    } else {
      this.mode = "new";
      this.testForm = {};
    }
  }

  submitTest() {
    let testId = null;
    if (this.mode === "edit") {
      testId = this.test._id;
    }

    this.TestService.submitTest(this.testForm, this.mode, testId)
      .then(data => {
        console.log(data);
        this.testForm = {};
        this.testId = null;
        this.onUpdate({ test: data.test });
        if (this.mode === "edit") {
          this.mode = "new";
          this.test = "null";
        }
      })
      .catch(e => {
        console.log("error updating test", e);
      });
  }
}

export default TestManagerController;
