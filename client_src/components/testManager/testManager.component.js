import template from "./testManager.html";
import controller from "./testManager.controller";

let testManagerComponent = {
  bindings: {
    test: "<",
    show: "<",

    onDelete: "&",
    onUpdate: "&"
  },
  template,
  controller,
  controllerAs: "vm"
};

export default testManagerComponent;
