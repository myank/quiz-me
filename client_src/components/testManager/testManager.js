import angular from "angular";
import testManagerComponent from "./testManager.component";
import questionListingComponent from "../questionlisting/questionlisting";
import questionManagerComponenet from "../questionManager/questionmanager";

const testManagerModule = angular
  .module("testManager", [questionManagerComponenet.name])
  .component("testManager", testManagerComponent);

export default testManagerModule;
