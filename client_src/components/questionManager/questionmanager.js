import angular from "angular";
import questionManagerComponent from "./questionmanager.component";

const questionManagerModule = angular
  .module("questionManager", [])
  .component("questionManager", questionManagerComponent);
export default questionManagerModule;
