import template from "./questionmanager.html";
import controller from "./questionmanager.controller";

let questionManagerComponent = {
  bindings: {
    question: "<",
    action: "<",
    onDelete: "&",
    onUpdate: "&"
  },
  template,
  controller,
  controllerAs: "vm"
};

export default questionManagerComponent;
