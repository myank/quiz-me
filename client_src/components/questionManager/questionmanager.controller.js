class QuestionManagerController {
  constructor(TestService) {
    this.TestService = TestService;
    this.questionForm = {};
  }
  $onInit() {}
  $onChanges(changes) {
    if (this.question) {
      console.log(this.question);
      this.questionForm.description = this.question.description;
      this.questionForm.option1 = this.question.options[0];
      this.questionForm.option2 = this.question.options[1];
      this.questionForm.option3 = this.question.options[2];

      this.questionForm.option4 = this.question.options[3];
      this.questionForm.answer = this.question.answer;
      this.questionForm._id = this.question._id;
      console.log(this.questionForm);
    }
  }
  $onDestroy() {
    this.questionForm = {};
    this.question = {};
    this.radioIds = [];
    console.log("printing on modal close");
  }

  updateEditForm() {}

  Question(action) {
    let method = "edit";
    this.questionForm.optionsArray = [
      this.questionForm.option1,
      this.questionForm.option2,
      this.questionForm.option3,
      this.questionForm.option4
    ];
    if (action === "new") {
      method = null;
    }
    console.log(this.questionForm);
    this.TestService.submitEditedQuestion(
      this.questionForm,
      method,
      this.questionForm._id
    )
      .then(data => {
        this.question = data.question;
        console.log(data.question);
        this.onUpdate({ question: this.question, action: action });
        this.questionForm = {};
      })
      .catch(e => {
        console.log("error editing question", e);
        this.questionForm = {};
      });
  }

  resetSubmitForm() {
    console.log("resetting quesiton from");
    this.questionForm = {};
  }
}
export default QuestionManagerController;
