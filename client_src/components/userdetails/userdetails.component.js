import template from "./userdetails.html";
import controller from "./userdetails.controller.js";

let userDetailsComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default userDetailsComponent;
