import angular from "angular";

import userDetailsComponent from "./userdetails.component";

const userDetailsModule = angular
  .module("userDetails", [])
  .component("userDetails", userDetailsComponent);
export default userDetailsModule;
