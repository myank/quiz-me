class UserDetailsController {
  constructor(UserService, TestService, $scope, moment) {
    "ngInject";
    this.UserService = UserService;
    this.TestService = TestService;
    this.$scope = $scope;
    this.$scope.labels = [];
    this.moment = moment;
    $(".modal").modal();
  }
  $onInit() {
    this.UserService.getUserData()
      .then(data => {
        console.log(data);
        this.user = data.User;
        if (data === "unauthorised") {
          this.$location.path("/");
        }
        this.user.tests.forEach(test => {
          test.percentageScore = test.score / test.test.questions.length * 100;
          test.attemptDate = this.moment(test.attemptDate).format("Do MMM YY");
        });
        console.log(this.user.tests);
      })
      .catch(e => {
        console.log(e);
      });
  }

  selectTest(test) {
    this.selectedTest = test;
    this.TestService.getTestResult(test._id)
      .then(data => {
        this.selectedTest = data.test;
      })
      .catch(e => {
        console.log("error fetching user test", e);
      });
  }
}

export default UserDetailsController;
