function TestService($http, $q) {
  "ngInject";

  return {
    //making a request to server for tests
    getTests() {
      let deferred = $q.defer();
      $http
        .get("/user/tests")
        .then(response => {
          console.log(response.data);
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
        });

      return deferred.promise;
    },
    getTestsAdmin() {
      let deferred = $q.defer();
      $http
        .get("/admin/tests")
        .then(response => {
          console.log(response.data);
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
        });
      return deferred.promise;
    },
    getTest(id) {
      let deferred = $q.defer();
      $http({
        method: "GET",
        url: "/test/" + id
      })
        .then(response => {
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
        });
      return deferred.promise;
    },
    getTestResult(test_id) {
      let deferred = $q.defer();
      $http({
        method: "GET",
        url: "/user/test/result/" + test_id
      })
        .then(response => {
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
        });
      return deferred.promise;
    },
    submitTest(details, mode, testId = null) {
      let url = "/admin/test";
      let method = "POST";
      if (mode === "edit") {
        url = "/admin/test/" + testId;
        method = "PATCH";
      }
      let deferred = $q.defer();
      $http({
        method: method,
        url: url,
        data: details
      })
        .then(response => {
          console.log("logging resposne in submit test admin", response);
          deferred.resolve(response.data);
        })
        .catch(e => {
          console.log("error saving test in db", e);
          deferred.reject(e);
        });
      return deferred.promise;
    },
    deleteTest(testId) {
      let deferred = $q.defer();
      $http({
        method: "DELETE",
        url: "/admin/test/" + testId
      })
        .then(response => {
          deferred.resolve(response.data);
          console.log("deleted test", response.data);
        })
        .catch(e => {
          console.log("error in deleting the test", e);
          deferred.reject(e);
        });
      return deferred.promise;
    },
    deleteQuestion(questionId) {
      let deferred = $q.defer();
      $http({
        method: "DELETE",
        url: "admin/question/" + questionId
      })
        .then(response => {
          deferred.resolve(response.data);
          console.log("logging response in delete question", response);
        })
        .catch(e => {
          deferred.reject(e);
          console.log("logging error in delete question", e);
        });
      return deferred.promise;
    },

    submitEditedQuestion(editedQuestion, action, question_id = null) {
      let deferred = $q.defer();
      let method = action === "edit" ? "PATCH" : "POST";
      let url =
        action === "edit"
          ? "/admin/question/" + question_id
          : "/admin/question";

      $http({
        method: method,
        url: url,
        data: editedQuestion
      })
        .then(response => {
          console.log(response);
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
        });
      return deferred.promise;
    },
    getQuestionList(url) {
      let deferred = $q.defer();
      $http({
        method: "GET",
        url: url
      })
        .then(response => {
          console.log(response);
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
          console.log("error fetching the questions", e);
        });
      return deferred.promise;
    }
  };
}
export default TestService;
