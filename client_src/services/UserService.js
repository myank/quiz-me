function UserService($http, $q) {
  "ngInject";
  let user = null;
  return {
    getUserData() {
      let deferred = $q.defer();
      $http
        .get("/user/profile")
        .then(response => {
          console.log(response);
          deferred.resolve(response.data);
        })
        .catch(error => {
          console.log(error);
          deferred.reject(error);
        });
      return deferred.promise;
    },
    getAdminData() {
      let deferred = $q.defer();
      $http
        .get("/admin/profile")
        .then(response => {
          console.log(response);
          deferred.resolve(response.data);
        })
        .catch(error => {
          console.log(error);
          deferred.reject(error);
        });
      return deferred.promise;
    },
    getUserList() {
      let deferred = $q.defer();
      $http({
        method: "GET",
        url: "/admin/getUserList"
      })
        .then(response => {
          console.log("logging user list", response);
          deferred.resolve(response.data.users);
        })
        .catch(e => {
          deferred.reject(e);
        });
      return deferred.promise;
    },

    isLoggedIn() {
      if (user) {
        return true;
      } else {
        return false;
      }
    },
    getUserStatus() {
      return $http
        .get("/userstatus")
        .then(response => {
          user = true;
        })
        .catch(error => {
          user = false;
        });
    },
    register(credentials) {
      let url = "/user/register";
      let deferred = $q.defer();
      if (credentials.admin_id) {
        url = "/admin/register/";
      }
      $http({
        method: "POST",
        url: url,
        data: credentials
      })
        .then(response => {
          console.log(response);
          user = true;
          let userRole = "user";
          if (response.data.admin_id) {
            userRole = "admin";
          }
          window.localStorage.UserRole = userRole;
          window.localStorage.username = response.data.username;
          deferred.resolve(response.data);
        })
        .catch(e => {
          deferred.reject(e);
          user = false;
        });
      return deferred.promise;
    },
    login(credentials) {
      let deferred = $q.defer();
      $http({
        method: "POST",
        url: "/login",
        data: credentials
      })
        .then(response => {
          user = true;
          console.log(response);
          let userRole = "user";
          if (response.data.admin_id) {
            userRole = "admin";
          }
          window.localStorage.UserRole = userRole;
          window.localStorage.username = response.data.username;
          deferred.resolve(response.data);
        })
        .catch(e => {
          user = false;
          console.log(e);
          deferred.reject(e);
        });
      return deferred.promise;
    },
    logoutUser() {
      let deferred = $q.defer();
      $http
        .get("/logout")
        .then(response => {
          console.log(response);
          deferred.resolve(response.data);
        })
        .catch(error => {
          console.log(error);
          deferred.reject(error);
        });
      return deferred.promise;
    },
    calculateAverages(tests) {
      console.log(tests);
      let AveragesArray = [];
      let currentAverage = 0;
      for (let i = 0; i < tests.length; i++) {
        currentAverage =
          (currentAverage * i +
            tests[i].score / tests[i].test.questions.length * 100) /
          (i + 1);

        console.log(currentAverage);
        AveragesArray.push(currentAverage);
      }
      console.log("logging averages Array", AveragesArray);
      return AveragesArray;
    },
    forgotPassword(credentials) {
      let deferred = $q.defer();
      $http({
        method: "POST",
        url: "/forgot-password",
        data: credentials
      })
        .then(response => {
          console.log("logging forgot password data", response);
          deferred.resolve(response.data);
        })
        .catch(e => {
          console.log("forgot password returned error", e);
          deferred.reject(e);
        });
      return deferred.promise;
    },
    changePassword(url, newPassword) {
      console.log(url, newPassword);
      let deferred = $q.defer();
      $http({
        method: "POST",
        url: url,
        data: {
          newPassword: newPassword
        }
      })
        .then(response => {
          console.log("logging response in reset password", response);
          deferred.resolve(response.data);
        })
        .catch(e => {
          console.log("error reseting the password", e);
          deferred.reject(e);
        });

      return deferred.promise;
    }
  };
}
export default UserService;
