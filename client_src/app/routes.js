function routes($routeProvider) {
  $routeProvider
    .when("/", {
      template: "<home></home>",
      access: {
        restricted: false
      }
    })
    .when("/test/:id", {
      template: "<test-dashboard></testdashboard>",
      access: {
        restricted: true,
        role: "user"
      }
    })
    .when("/endTest", {
      template: "<end-test></end-test>",
      access: {
        restricted: true,
        role: "user"
      }
    })
    .when("/user/dashboard", {
      template: "<user-dashboard></user-dashboard>",
      access: {
        restricted: true,
        role: "user"
      }
    })
    .when("/user/test/result/:test_id", {
      template: "<test-result></test-result>",
      access: {
        restricted: true,
        role: "user"
      }
    })
    .when("/admin/dashboard", {
      template: "<admin-dashboard></admin-dashboard>",
      access: {
        restricted: true,
        role: "admin"
      }
    })
    .when("/reset-password/:token/:userType", {
      template: "<reset-password></reset-password>",
      access: {
        restricted: false
      }
    })
    .otherwise({
      redirectTo: "/",
      access: {
        restricted: false
      }
    });
}

export default routes;
