function authRouteCheck($rootScope, $location, $route, UserService) {
  $rootScope.$on("$routeChangeStart", function(event, next, current) {
    if (next.access.restricted) {
      UserService.getUserStatus().then(() => {
        let UserRole = window.localStorage.UserRole;
        if (!UserService.isLoggedIn()) {
          console.log("restricted");
          $location.path("/");
          $route.reload();
        }
        if (UserService.isLoggedIn() && next.access.role != UserRole) {
          console.log("userrole not matching");
          console.log(UserRole);
          $location.path("/");
          $route.reload();
        }
      });
    }
  });
}

export default authRouteCheck;
