import "../../node_modules/materialize-css/dist/js/materialize";
import angular from "angular";
import ngRoute from "angular-route";

import moment from "moment";
import angularMoment from "angular-moment";
import "../../node_modules/angular-css/angular-css";
import "../../node_modules/materialize-css/dist/css/materialize.min.css";
import angularMaterialize from "angular-materialize";
import angularLoadingBar from "angular-loading-bar";
import "../../node_modules/angular-loading-bar/build/loading-bar.min.css";

import AppComponent from "./app.component";
import HomeComponent from "../pages/landing/home";
import TestService from "../services/TestService";

import routeAuthCheck from "./routeAuthCheck";
import TestDashboardComponent from "../pages/testDashboard/testDashboard";

import UserService from "../services/UserService";
import UserDashboard from "../pages/userdashboard/userdashboard";
import AdminDashboard from "../pages/admindashboard/admindashboard";
import testResultComponent from "../components/testResult/testResult";
import resetPasswordComponent from "../pages/resetPassword/resetPassword";
import "./main.css";

angular
  .module("app", [
    ngRoute,
    angularMaterialize,
    angularMoment,
    angularLoadingBar,
    HomeComponent.name,
    AdminDashboard.name,
    UserDashboard.name,
    TestDashboardComponent.name,
    testResultComponent.name,
    resetPasswordComponent.name
  ])
  .config([
    "cfpLoadingBarProvider",
    function(cfpLoadingBarProvider) {
      cfpLoadingBarProvider.includeSpinner = false;
    }
  ])
  .config([
    "$routeProvider",
    function($routeProvider) {
      $routeProvider
        .when("/", {
          template: "<home></home>",
          access: {
            restricted: false
          }
        })
        .when("/test/:id", {
          template: "<test-dashboard></testdashboard>",
          access: {
            restricted: true,
            role: "user"
          }
        })
        .when("/endTest", {
          template: "<end-test></end-test>",
          access: {
            restricted: true,
            role: "user"
          }
        })
        .when("/user/dashboard", {
          template: "<user-dashboard></user-dashboard>",
          access: {
            restricted: true,
            role: "user"
          }
        })
        .when("/user/test/result/:test_id", {
          template: "<test-result></test-result>",
          access: {
            restricted: true,
            role: "user"
          }
        })
        .when("/admin/dashboard", {
          template: "<admin-dashboard></admin-dashboard>",
          access: {
            restricted: true,
            role: "admin"
          }
        })
        .when("/reset-password/:token/:userType", {
          template: "<reset-password></reset-password>",
          access: {
            restricted: false
          }
        })
        .otherwise({
          redirectTo: "/",
          access: {
            restricted: false
          }
        });
    }
  ])
  .run(routeAuthCheck)
  .component("app", AppComponent)
  .factory("TestService", TestService)
  .factory("UserService", UserService);
