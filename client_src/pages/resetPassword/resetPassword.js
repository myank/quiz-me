import angular from "angular";
import resetPasswordComponent from "./resetPassword.component";

const resetPasswordModule = angular
  .module("resetPassword", [])
  .component("resetPassword", resetPasswordComponent);

export default resetPasswordModule;
