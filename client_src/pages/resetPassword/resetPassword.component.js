import template from "./resetPassword.html";
import controller from "./resetPassword.controller";

let resetPasswordComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default resetPasswordComponent;
