class ResetPasswordController {
  constructor(UserService, $location, $routeParams) {
    this.UserService = UserService;
    this.$location = $location;
    this.$routeParams = $routeParams;
  }

  resetPassword() {
    console.log(this.$routeParams.token);
    let url =
      "/reset-password/" +
      this.$routeParams.token +
      "/" +
      this.$routeParams.userType;
    this.UserService.changePassword(url, this.newPassword)
      .then(data => {
        console.log(data);
        this.$location.path("/");
      })
      .catch(e => {
        console.log("error in resetting the password.");
      });
  }
}
export default ResetPasswordController;
