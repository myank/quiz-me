import template from "./home.html";
import controller from "./home.controller";
import css from "./home.css";
let homeComponent = {
  bindings: {},
  css,
  template,
  controller,
  controllerAs: "vm"
};

export default homeComponent;
