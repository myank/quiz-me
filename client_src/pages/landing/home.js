import angular from "angular";
import homeComponent from "./home.component";
import SignInComponent from "../../components/signIn/signIn";

const homeModule = angular
  .module("home", [SignInComponent.name])
  .component("home", homeComponent);

export default homeModule;
