class HomeController {
  constructor() {
    "ngInject";
    this.name = "home";
    $(".modal").modal();
    $(".button-collapse").sideNav({
      menuWidth: 300,
      edge: "left",
      closeOnClick: true,
      draggable: true
    });
  }
  $onInit() {
    this.userTypeDefault = "User";
    this.actionDefault = "register";
  }
  loadForm(userType, action) {
    this.userType = userType;
    this.action = action;
  }
  $onDestroy() {
    $(".modal").modal("close");
  }
}

export default HomeController;
