import template from "./admindashboard.html";
import controller from "./admindashboard.controller";

let adminDashboardComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: "vm"
};

export default adminDashboardComponent;
