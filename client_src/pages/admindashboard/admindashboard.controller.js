class adminDashboardController {
  constructor(UserService, $location) {
    "ngInject";
    this.UserService = UserService;
    this.$location = $location;
    $(".tabs").tabs();
    $(".button-collapse").sideNav({
      menuWidth: 300,
      edge: "left",
      closeOnClick: true,
      draggable: true
    });
  }

  $onInit() {
    this.UserService.getAdminData()
      .then(data => {
        console.log(data);
        this.admin = data;
        if (data === "unauthorised") {
          this.$location.path("/");
        }
      })
      .catch(e => {
        console.log(e);
      });
    this.views = [];

    for (let i = 0; i < 3; i++) {
      let viewObj = {
        show: i === 0 ? true : false
      };
      this.views.push(viewObj);
    }
  }

  toggleView(n) {
    for (let i = 0; i < this.views.length; i++) {
      if (i === n) {
        this.views[i].show = true;
      } else {
        this.views[i].show = false;
      }
    }
    console.log(this.views);
  }

  logout() {
    this.UserService.logoutUser()
      .then(data => {
        if (data === "loggedout") {
          this.$location.path("/");
        }
      })
      .catch(e => {
        console.log(e);
      });
  }
}

export default adminDashboardController;
