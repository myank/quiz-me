import angular from "angular";
import adminDashboardComponent from "./admindashboard.component";
import testListingAdminComponent from "../../components/testlistingadmin/testlistingadmin";
import userListingAdminComponent from "../../components/userlistingadmin/userlistingadmin";
const adminDashboardModule = angular
  .module("adminDashboard", [
    testListingAdminComponent.name,
    userListingAdminComponent.name
  ])

  .component("adminDashboard", adminDashboardComponent);

export default adminDashboardModule;
