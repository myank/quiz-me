import io from "socket.io-client";

class TestDashboardController {
  constructor($routeParams, $scope, $location) {
    //this will keep the service instance across our class.
    this.$scope = $scope;
    this.$routeParams = $routeParams;
    this.socket = undefined;
    this.$location = $location;
    $(".modal").modal();
  }

  $onDestroy() {
    this.socket.disconnect();
    console.log("testSocketDisconnect was fired.");
  }
  $onInit() {
    this.socket = io.connect();

    this.socket.emit("startTest", this.$routeParams.id);
    this.socket.on("sendTest", (data, cb) => {
      console.log(data);
      this.test = data.test;

      this.dispQuestion = this.test.questions[0];
      this.userTestId = data.userTestId;
      this.$scope.$apply();

      cb();
    });
    this.socket.on("time", time => {
      this.dispTime = time;
      this.$scope.$apply();
    });

    this.socket.on("testEnded", data => {
      this.$scope.$apply(() => {
        this.$location.path("/user/test/result/" + this.userTestId);
      });
    });
    this.AnsweredQuestions = [];
  }
  submitAnswer(question_id, userAnswer) {
    console.log("logging question id and user answer", question_id, userAnswer);
    this.socket.emit(
      "submitAnswer",
      {
        question_id: question_id,
        answer: userAnswer,
        time: this.dispTime
      },
      data => {
        console.log(data);
        // this.showQuestion(question_id, "next");
        let qIndex = this.test.questions.findIndex(x => x._id === data);
        console.log(qIndex);
        this.test.questions[qIndex].answered = true;
        console.log(this.test.questions);
        this.$scope.$apply();
      }
    );
  }
  // showQuestion(question_id, to) {
  //   let qIndex = this.test.questions.findIndex(x => x._id === question_id);
  //   console.log(qIndex);
  //   if (to === "next") {
  //     if (qIndex < this.test.questions.length - 1) {
  //       this.dispQuestion = this.test.questions[qIndex + 1];
  //     } else {
  //       this.dispQuestion = this.test.questions[0];
  //     }
  //   } else if (to === "previous") {
  //     this.dispQuestion = this.test.questions[qIndex - 1];
  //   } else if (to === "id") {
  //     console.log(question_id);
  //     this.dispQuestion = this.test.questions[qIndex];
  //     console.log(this.dispQuestion);
  //   }
  // }
  endTest() {
    //TODO: Add logic to show modal when user tries to submit the test before time is finished.
    this.socket.emit("endTestManual", {});
    console.log("userTestId", this.userTestId);
    this.$location.path("/user/test/result/" + this.userTestId);

    console.log("endTest event fired");
  }
  isSelected(question) {
    if (this.dispQuestion._id === question._id) {
      return "btn-primary";
    } else if (question.answered) {
      return "btn-success";
    } else {
      return "btn-outline-primary";
    }
  }
  resetAnswer(question_id) {
    console.log(question_id);
    this.socket.emit("resetAnswer", question_id, () => {
      console.log(
        "received the call back from server for resetting the question on server"
      );

      // this.showQuestion(question_id, "next");
      let qIndex = this.test.questions.findIndex(x => x._id === question_id);
      console.log(qIndex);
      this.test.questions[qIndex].answered = false;
      this.test.questions[qIndex].userAnswer = "";
      console.log(this.test.questions);
      this.$scope.$apply();

      console.log(this.test);
    });
  }
  changeAtIndex(arr, id, property, value) {
    let index = arr.findIndex(x => x._id === id);
    console.log(index);
    console.log(arr[index]);
    arr[index][property] = value;
  }
  highlightAnswer(option) {
    if (
      this.dispQuestion.answeredOption &&
      this.dispQuestion.answeredOption === option
    ) {
      return "selected-option";
    }
  }
}
export default TestDashboardController;
