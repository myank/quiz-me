import angular from "angular";
import testDashboardComponent from "./testDashboard.component";
const testDashboardModule = angular
  .module("testDashboard", [])
  .component("testDashboard", testDashboardComponent);
export default testDashboardModule;
