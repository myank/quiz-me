import template from "./testDashboard.html";
import controller from "./testDashboard.controller";
import css from "./testDashboard.style.css";

let testDashboardComponent = {
  restrict: "E",
  bindings: {},
  css,
  template,
  controller,
  controllerAs: "vm"
};
export default testDashboardComponent;
