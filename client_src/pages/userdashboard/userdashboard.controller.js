class UserDashboardController {
  constructor(UserService, $location, moment, $scope) {
    "ngInject";
    this.UserService = UserService;
    this.$location = $location;
    this.moment = moment;
    this.$scope = $scope;
    this.$scope.labels = [];
    $(".tabs").tabs();
    $(".button-collapse").sideNav({
      menuWidth: 300,
      edge: "left",
      closeOnClick: true,
      draggable: true
    });
  }

  $onInit() {
    this.UserService.getUserData()
      .then(data => {
        console.log(data);
        this.user = data.User;
        if (data === "unauthorised") {
          this.$location.path("/");
        }
        this.user.tests.forEach(test => {
          test.attemptDate = this.moment(test.attemptDate).format("Do MMM YY");

          this.$scope.labels.push(test.attemptDate);
        });

        this.$scope.data = this.UserService.calculateAverages(this.user.tests);
        this.dispAveragePercentage =
          this.$scope.data.length > 6
            ? this.$scope.data.slice(
                this.$scope.data.length - 6,
                this.$scope.data.length
              )
            : this.$scope.data;
      })
      .catch(e => {
        console.log(e);
      });
    this.views = [];
    for (let i = 0; i < 3; i++) {
      let viewObj = {
        show: i === 0 ? true : false
      };
      this.views.push(viewObj);
    }
  }

  toggleView(n) {
    for (let i = 0; i < this.views.length; i++) {
      if (i === n) {
        this.views[i].show = true;
      } else {
        this.views[i].show = false;
      }
    }
    console.log(this.views);
  }

  logout() {
    this.UserService.logoutUser()
      .then(data => {
        if (data === "loggedout") {
          this.$location.path("/");
        }
      })
      .catch(e => {
        console.log(e);
      });
  }
}
export default UserDashboardController;
