import angular from "angular";
import userDashboardComponent from "./userdashboard.component";
import testListingComponent from "../../components/testlisting/testlisting";
import userDetailsComponent from "../../components/userdetails/userdetails";
import "chart.js";
import "angular-chart.js";

const userDashboardModule = angular
  .module("userDashboard", [
    "chart.js",
    testListingComponent.name,
    userDetailsComponent.name
  ])
  .component("userDashboard", userDashboardComponent);

export default userDashboardModule;
