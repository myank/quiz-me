const passport = require("passport"),
  GoogleStrategy = require("passport-google-oauth20"),
  LocalStrategy = require("passport-local"),
  User = require("../models/user.model");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((user, done) => {
  if (user.admin_id) {
    User.Admin.findById(user._id).then(user => {
      done(null, user);
    });
  } else {
    User.User.findById(user._id).then(user => {
      done(null, user);
    });
  }
});
passport.use(
  new GoogleStrategy(
    {
      //optopm fpr the google strategy.
      callbackURL: "/auth/google/redirect",
      clientID: process.env.googleClientID,
      clientSecret: process.env.googleClientSecret
    },
    (accessToken, refreshToken, profile, done) => {
      //passport callback function

      //check if use already exists in our database.
      User.User.findOne({ google_id: profile.id }).then(currentUser => {
        if (currentUser) {
          //already found user
          console.log("found user in database", currentUser);
          done(null, currentUser);
        } else {
          //create a new user in db
          new User.User({
            username: profile.emails[0].value,
            google_id: profile.id,
            email: profile.emails[0].value
          })
            .save()
            .then(newUser => {
              done(null, newUser);
            });
        }
      });
    }
  )
);

//new local strategy for authentication
passport.use(
  new LocalStrategy(
    { usernameField: "email", passwordField: "password" },
    (email, password, done) => {
      return User.User.findByCredentials(email, password)
        .then(user => {
          if (!user) {
            return User.Admin.findByCredentials(email, password).then(admin => {
              if (!admin) {
                return done(null, false, {
                  message: "Incorrect e-mail address or password."
                });
              }
              return done(null, admin);
            });
          }
          return done(null, user);
        })
        .catch(err => {
          return done(err);
        });
    }
  )
);

let sessionMiddleware = session({
  secret: process.env.cookieKey,
  saveUninitialized: true,
  resave: true,
  store: new MongoStore({
    url: process.env.MONGODB_URI
  })
});
module.exports = { sessionMiddleware };
