// const config = require("./config/config");

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();
const http = require("http").createServer(app);
const port = process.env.PORT;
const path = require("path");
const publicPath = path.join(__dirname, "../client_dist");
const questionModel = require("./models/question.model");
const testModel = require("./models/test.model");
const passport = require("passport");
const passportSetup = require("./config/passport.setup");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(publicPath));

app.use(passportSetup.sessionMiddleware);
// initialize passport
app.use(passport.initialize());
app.use(passport.session());
mongoose.connect(process.env.MONGODB_URI);

let quizRoutes = require("./controllers/quiz.controller");
let userRoutes = require("./controllers/user.controller");
app.use("/", quizRoutes);
app.use("/", userRoutes);
http.listen(port, () => {
  console.log(`listening to port ${port}`);
});

const quizSocket = require("./lib/quiz.sockets");
quizSocket.controller(http);
module.exports = { app };
