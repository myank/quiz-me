const express = require("express");
const router = express.Router();
const Question = require("../models/question.model");
const Test = require("../models/test.model");
const Subject = require("../models/subject.model");
const { ObjectID } = require("mongodb");
const _ = require("lodash");
const Auth = require("./middlewares/auth");

//POST /admin/test route
router.post("/admin/test", Auth.isAdmin, (req, res) => {
  //use batchInsert to add questions to the questions schema
  let body = _.pick(req.body, ["testname", "timelimit"]);
  let newTest = new Test({
    testname: req.body.testname,

    timelimit: req.body.timelimit
  });
  return newTest.save().then(test => {
    res.send({ test });
  });
});

//GET /test route get all tests.
router.get("/user/tests", Auth.AuthCheck, (req, res) => {
  Test.find({})
    .populate("questions", "description options")
    .then(
      tests => {
        res.send({ tests });
      },
      e => {
        console.log("error fetching test for user", e);
        res.status(400).send();
      }
    );
});

//GET /test route to get all tests for admin.
router.get("/admin/tests", Auth.isAdmin, (req, res) => {
  Test.find({})
    .populate("questions")
    .then(
      tests => {
        res.send({ tests });
      },
      e => {
        console.log("error fetching test for admin", e);
        res.status(400).send();
      }
    );
});
//GET /admin/test/:id route to get a particular test
router.get("/test/:id", Auth.AuthCheck, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send("not valid id");
  }
  Test.findById(id)
    .populate("questions")
    .lean()
    .then(test => {
      if (!test) {
        return res.status(404).send("no test with this id exisits");
      }
      return Question.find({})
        .lean()
        .then(questions => {
          for (let i = 0; i < test.questions.length; i++) {
            let index = questions.findIndex(function(x) {
              return (
                x._id.toHexString() === test.questions[i]._id.toHexString()
              );
            });

            if (index > -1) {
              questions[index].selected = true;
              test.questions[i].selected = true;
            }
          }
          console.log(test, questions);

          res.send({ test, questions });
        });
    })
    .catch(e => {
      console.log(e);
      res.status(400).send(e);
    });
});

//DELETE /admin/test/:id route to delete a particular test.
router.delete("/admin/test/:id", Auth.isAdmin, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send("not valid id");
  }
  Test.findByIdAndRemove(id)
    .then(test => {
      if (!test) {
        return res.status(404).send("test not found");
      }
      res.send({ test });
    })
    .catch(e => {
      res.status(400).send();
    });
});

//PATCH /admin/test/:id route to edit a particular test.
router.patch("/admin/test/:id", Auth.isAdmin, (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ["testname", "questions", "timelimit"]);

  if (!ObjectID.isValid(id)) {
    return res.status(404).send("not valid id");
  }

  Test.findByIdAndUpdate(id, { $set: body }, { new: true })
    .then(test => {
      if (!test) {
        return res.status(404).send("test not found");
      }
      res.send({ test });
    })
    .catch(e => {
      console.log(e);
      res.status(400).send(e);
    });
});

router.patch("/admin/test/addQuestion/:id", Auth.isAdmin, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send("not valid id");
  }
  Test.findByIdAndUpdate(id, { $push: { questions: req.body.questions } })
    .then(test => {
      if (!test) {
        return res.status(404).send("test not found");
      }
      res.send({ test });
    })
    .catch(e => {
      res.status(400).send(e);
    });
});

router.patch("/admin/test/removeQuestion/:id", Auth.isAdmin, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send("not valid id");
  }
  Test.findByIdAndUpdate(id, { $pull: { questions: req.body.questions } })
    .then(test => {
      if (!test) {
        return res.status(404).send();
      }
      res.send({ test });
    })
    .catch(e => {
      res.status(400).send();
    });
});

//routes to add question
router.post("/admin/question", Auth.isAdmin, (req, res) => {
  console.log(req.body);
  let newQuestion = new Question({
    description: req.body.description,
    answer: req.body.answer,
    options: req.body.optionsArray
  });
  newQuestion.save().then(
    question => {
      Subject.findByIdAndUpdate(req.body.subjectId, {
        $push: { questions: question._id }
      }).then(subject => {
        res.send({ question });
      });
    },
    e => {
      res.status(400).send(e);
    }
  );
});

//route to get questions
router.get("/admin/question", Auth.isAdmin, (req, res) => {
  Question.find({}).then(
    questions => {
      res.send({ questions });
    },
    e => {
      res.status(400).send();
    }
  );
});

//route to get questions by subject
router.get("/admin/question/:subject_id", Auth.isAdmin, (req, res) => {
  Subject.findById(req.params.subject_id)
    .populate("questions")
    .then(subject => {
      if (!Subject) {
        return res.status(404).send("This subject does not exist in the db");
      }
      res.send({ subject });
    })
    .catch(e => {
      res.status(400).send();
    });
});

//route to edit a question
router.patch("/admin/question/:id", Auth.isAdmin, (req, res) => {
  console.log(req.body);
  let id = req.params.id;
  let body = _.pick(req.body, ["description", "answer"]);
  body.options = req.body.optionsArray;
  console.log(body);
  if (!ObjectID.isValid(id)) {
    return res.status(404).send("id not valid");
  }
  Question.findByIdAndUpdate(id, { $set: body }, { new: true })
    .then(question => {
      if (!question) {
        return res.status(404).send("question not found");
      }
      console.log("question", question);
      res.send({ question });
    })
    .catch(e => {
      console.log(e);
      res.status(400).send(e);
    });
});

//route to delete a question
router.delete("/admin/question/:id", Auth.isAdmin, (req, res) => {
  let id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }
  Question.findByIdAndRemove(id)
    .then(question => {
      if (!question) {
        return res.status(404).send();
      }
      res.send({ question });
    })
    .catch(e => {
      res.status(400).send();
    });
});

//route to add a subject.
router.post("/admin/subject", Auth.isAdmin, (req, res) => {
  let body = _.pick(req.body, ["subjectName", "questions"]);
  let newSubject = new Subject(body);
  newSubject.save((err, subject) => {
    if (err) {
      return res.status(500).send();
    }
    return res.send(subject);
  });
});
//route to edit a subject.
router.patch("/admin/:subject_id", Auth.isAdmin, (req, res) => {
  let body = _.pick(req.body, ["subjectName", "questions"]);
  Subject.findByIdAndUpdate(
    req.params.subject_id,
    body,
    (err, updatedSubject) => {
      if (err) {
        console.log("error editing test", e);
        return res.status(400).send("bad request");
      }
      if (!updatedSubject) {
        return res.status(404).send("subject not found");
      }
      return res.status(200).send(updatedSubject);
    }
  );
});
//route to delete a subject
router.delete("/admin/:subject_id", Auth.isAdmin, (req, res) => {
  Subject.findByIdAndRemove(req.params.subject_id, (err, subject) => {
    if (err) {
      return res.status(400).send();
    }
    if (!subject) {
      return res.status(404).send();
    }
    return res.status(200).send();
  });
});
// route to get all subjects
router.get("/admin/subjects", Auth.isAdmin, (req, res) => {
  Subject.find({}, (err, subjects) => {
    if (err) {
      return res.status(400).send("error fetching subject");
    }
    if (!subjects) {
      return res.status(404).send();
    }
    return res.status(200).send({ subjects });
  });
});

module.exports = router;
