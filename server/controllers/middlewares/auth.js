let AuthCheck = function(req, res, next) {
  if (!req.isAuthenticated()) {
    //executes if user is not logged in.

    res.status(401).send("unauthorised");
  } else {
    //if logged in.

    next();
  }
};
let isAdmin = function(req, res, next) {
  if (req.isAuthenticated() && req.user.admin_id) {
    next();
  } else {
    res.status(401).send("unauthorised");
  }
};
module.exports = {
  AuthCheck,
  isAdmin
};
