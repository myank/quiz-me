const router = require("express").Router();
const passport = require("passport");
const Auth = require("./middlewares/auth");
const User = require("../models/user.model");
const UserTests = require("../models/user.test.model");
const _ = require("lodash");
const { ObjectID } = require("mongodb");

router.get(
  "/signin/google",
  passport.authenticate(
    "google",
    {
      scope: ["profile", "email"]
    },
    (req, res) => {
      res.header("Access-Control-Allow-Origin", "*").send();
    }
  )
);

//callback route for google to redirect ato
router.get(
  "/auth/google/redirect",
  passport.authenticate("google"),
  (req, res) => {
    console.log("logging req.user in auth/google/redirect", req.user);

    return res.redirect("/#!/user/dashboard");
  }
);
//route to register new user on for local authentication
router.post("/user/register", (req, res) => {
  if (req.body.username && req.body.email && req.body.password) {
    let newUser = new User.User(
      _.pick(req.body, ["username", "email", "password"])
    );
    newUser.save(err => {
      if (err) {
        return res.status(400).send(err);
      }
      req.logIn(newUser, err => {
        if (err) {
          return res.status(401).send(err);
        }
        return res.status(200).send(req.user);
      });
    });
  } else {
    return res.status(400).send("Some fields missing");
  }
});
router.post("/admin/register", (req, res) => {
  if (
    req.body.admin_id &&
    req.body.username &&
    req.body.email &&
    req.body.password
  ) {
    let newAdmin = new User.Admin(
      _.pick(req.body, ["admin_id", "username", "email", "password"])
    );
    newAdmin.save(err => {
      if (err) {
        return res.status(400).send(err);
      }
      req.logIn(newAdmin, err => {
        if (err) {
          return res.status(401).send(err);
        }
        return res.status(200).send(req.user);
      });
    });
  } else {
    return res.status(400).send("Some fields missing");
  }
});
//route to login the user
router.post("/login", (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      if (err === "Loginerr2") {
        console.log("returning error from 401 block", err);
        return res.status(401).send({ err });
      } else {
        console.log("returning error from 500 block", err);
        return res.status(500).send({ err });
      }
    }
    if (!user) {
      console.log(info);
      return res.status(401).send({ info });
    }
    req.login(user, err => {
      if (err) {
        console.log("returning error from 500 block", err);
        return res.status(500).send({ err });
      }
      //redirect use to #!/testdashboard
      return res.status(200).send(req.user);
    });
  })(req, res, next);
});

//route to log out the user
router.get("/logout", Auth.AuthCheck, (req, res) => {
  //log user out from the network;
  req.logout();
  res.send("loggedout");
});

router.get("/user/profile", Auth.AuthCheck, (req, res) => {
  User.User.findById(req.user._id)
    .populate("tests")
    .then(User => {
      res.send({ User });
    })
    .catch(e => {
      console.log("error fetching user data /user/profile", e);
    });
});

router.get("/admin/profile", Auth.AuthCheck, (req, res) => {
  User.Admin.findById(req.user._id)
    .then(admin => {
      if (!admin) {
        return res.status(404).send();
      }
      return res.status(200).send(admin);
    })
    .catch(e => {
      console.log("error in fetchin admin profile", e);
      return res.status(400).send();
    });
});

router.get("/admin/getUserList", Auth.isAdmin, (req, res) => {
  User.User.find({})
    .populate("tests")
    .then(users => {
      if (!users) {
        return res.status(404).send("users list not found or empty");
      }
      res.status(200).send({ users });
    })
    .catch(e => {
      console.log("error admin/getUserList", e);
      return res.status(400).send();
    });
});

//route to get test result of specific test of user;
router.get("/user/test/result/:testId", Auth.AuthCheck, (req, res) => {
  UserTests.findById(req.params.testId)
    .populate({ path: "test" })
    .populate({
      path: "answerKey",
      populate: { path: "question", select: "description options" }
    })
    .lean()
    .then(test => {
      if (!test) {
        return res.status(404).send();
      }
      return res.status(200).send({ test });
    })
    .catch(e => {
      console.log("logging error in user/test/result route", e);
      return res.status(400).send();
    });
});

//route to get user status i.e. user is logged in ornot.
router.get("/userstatus", (req, res) => {
  console.log("get user status fired");
  if (!req.isAuthenticated()) {
    return res.status(401).send(false);
  } else {
    res.status(200).send(true);
  }
});

router.post("/forgot-password", (req, res) => {
  User[req.body.userType]
    .createResetPasswordToken(req.body.email, req.headers.host)
    .then(() => {
      res.send(true);
    })
    .catch(e => {
      res.status(400).send(e);
    });
});
router.get("/reset-password/:token/:userType", (req, res) => {
  res.redirect(
    "/#!/reset-password/" + req.params.token + "/" + req.params.userType
  );
});
router.post("/reset-password/:token/:userType", (req, res) => {
  console.log(req.body);
  let userType = req.params.userType;
  User[userType]
    .changePassword(req.params.token, req.body.newPassword)
    .then(() => {
      res.send("password changed successfully");
    })
    .catch(e => {
      console.log("error in changing the password", e);
      res.status(400).send();
    });
});
module.exports = router;
