const expect = require('expect');
const request = require('supertest');

const {app} = require('../../server');
const {ObjectID} = require('mongodb');
const Test = require('../../models/test.model');
const question = require('../../models/question.model');
const q = require('q');


const questionSet = [
    {
        _id : new ObjectID(),
        description : "this is the i th qeustion",
        answer : "gravity1",
        options: ['hocus pocus', 'gravity1', 'london system', 'sicilian'],
        subject : 'Chess'
    },
    {
        _id : new ObjectID(),
        description : "this is the i+n th qeustion",
        answer : "gravity3",
        options: ['hocus pocus', 'gravity3', 'london system', 'sicilian'],
        subject : 'QuantumChess'

    },
    {
        _id : new ObjectID(),
        description : "this is the i for test no.2 th qeustion",
        answer : "gravity2",
        options: ['hocus pocus', 'gravity2', 'london system', 'sicilian'],
        subject : 'QuantumChess'
    },
    {
        _id : new ObjectID(),
        description : "this is the i+n th for test no. 2 qeustion",
        answer : "gravity4",
        options: ['hocus pocus', 'gravity4', 'london system', 'sicilian'],
        subject : 'QuantumChess'
    }
]

const tests = [{
        _id : new ObjectID(),
        testname : "first test test testname",
        questionSet : [questionSet[0]._id, questionSet[1]._id],
        timelimit : 500
    },
    {
        _id : new ObjectID(),
        testname : "second test test name",
        questionSet : [questionSet[2]._id, questionSet[3]._id],
        timelimit : 600
    }
]

beforeEach((done)=>{
    let promise1 = Test.remove({}).then(()=>{
       return Test.insertMany(tests);
    });
    let promise2 = Question.remove({}).then(()=>{
        return Question.insertMany(questionSet)
    })
     q.all([promise1, promise2]).then(()=>{
         done();
     })
});

describe('POST /admin/test', () =>{

    it('should create a new test', (done)=>{
        let test = {
            testname : "third test testname",
            questionSet : [{
                    description : "this is the i th qeustion",
                    answer : "gravity",
                    options : ['hocus pocus', 'gravity', 'london system', 'sicilian']
                },
                {
                    description : "this is the i th qeustion",
                    answer : "gravity",
                    options: ['hocus pocus', 'gravity', 'london system', 'sicilian']
                },
            ]
        }

        request(app)
            .post('/admin/test')
            .send(test)
            .expect(200)
            .expect((res)=>{
                expect(res.body.testname).toBe(test.testname)
            })
            .end((err, res)=>{
                if(err){
                    return done(err);
                }
                Test.find({'testname' : test.testname}).then((tests)=>{
                    expect(tests.length).toBe(1);
                    expect(tests[0].testname).toBe(test.testname);
                    done();
                }).catch((e)=>{
                    done(e);
                })
            })
    })

    it('should not create new test with invalid body data', (done)=>{
        request(app)
            .post('/admin/test')
            .expect(400)
            .end((err, res)=>{
                if(err){
                    return done(err)
                }
                Test.find().then((tests)=>{
                    expect(tests.length).toBe(2);
                    done();
                }).catch((e)=>{
                    done(e);
                })
            })
    })

})

describe('GET /admin/test', ()=>{
    it('should get all tests', (done)=>{
        request(app)
            .get('/admin/test')
            .expect(200)
            .expect((res)=>{
                expect(res.body.tests.length).toBe(2)
            })
            .end(done);
    })
})

describe('GET /admin/test/:id', ()=>{
    it('should return test doc', (done)=>{
        console.log(tests[0]._id);
        request(app)
            .get(`/admin/test/${tests[0]._id.toHexString()}`)
            .expect(200)
            .expect((res)=>{
                expect(res.body.test.testname).toBe(tests[0].testname)
            })
            .end(done);
    })

    it('should return 404 if todo not found', (done)=>{
        let id = new ObjectID().toHexString();
        request(app)
            .get(`/admin/test/${id}`)
            .expect(404)
            .end(done);
    })

    it('should return 404 for non object ids', (done)=>{
        request(app)
            .get('/admin/test/1234')
            .expect(404)
            .end(done);
    })
})

describe('DELETE /admin/test/:id', ()=>{
    it('should remove a test', (done)=>{
        let hexId = tests[1]._id.toHexString();

        request(app)
            .delete(`/admin/test/${hexId}`)
            .expect(200)
            .expect((res)=>{
                expect(res.body.test._id).toBe(hexId);
            })
            .end((err, res)=>{
                if(err){
                    return done (err);
                }
                Test.findById(hexId).then((test)=>{
                    expect(test).toNotExist();
                    done();
                }).catch((e)=>{
                    done();
                })
            })
    })

    it('should return 404 if todo not found', (done)=>{
        let id = new ObjectID().toHexString();
        request(app)
            .delete(`/admin/test/${id}`)
            .expect(404)
            .end(done);
    })

    it('should return 404 for non object ids', (done)=>{
        request(app)
            .delete('/admin/test/1234')
            .expect(404)
            .end(done);
    })
})

describe('PATCH /admin/test/:id', ()=>{
    it('should update the tests', (done)=>{
        let id = tests[0]._id.toHexString();
        let test = {
            testname: "first test test testname(edit)",
            questionSet: [questionSet1[1]._id],
            timelimit: 502
        }
        request(app)
            .patch(`/admin/test/${id}`)
            .send({testname : 'edited', timelimit : 2323})
            .expect(200)
            .expect((res)=>{
                expect(res.body.test).toInclude({testname : 'edited', timelimit: 2323})
            })
            .end(done);
    })
})

