const mongoose = require("mongoose");

let testSchema = new mongoose.Schema({
  testname: {
    type: String,
    required: true
  },
  questions: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Question"
    }
  ],
  timelimit: Number
});

module.exports = mongoose.model("Test", testSchema);
