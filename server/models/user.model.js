const bcrypt = require("bcryptjs"),
  mongoose = require("mongoose"),
  validator = require("validator"),
  crypto = require("crypto"),
  nodemailer = require("nodemailer"),
  _ = require("lodash"),
  Test = require("./test.model"),
  jwtSecret = "Random_Secret";

let UserSchema = function(add) {
  let Schema = new mongoose.Schema({
    username: {
      type: String,
      required: true,
      unique: true
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: validator.isEmail,
        message: "{VALUE} is not a valid e-mail"
      }
    },
    password: {
      type: String,
      minlength: 6
    },
    google_id: {
      type: String
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date
  });
  Schema.methods.toJSON = function() {
    let user = this.toObject();
    delete user["password"];
    return user;
  };
  Schema.pre("save", function(next) {
    let user = this;
    if (!user.isModified("password")) return next();
    bcrypt.genSalt(10, (err, salt) => {
      let password = user.password;
      bcrypt.hash(password, salt, (err, hash) => {
        user.password = hash;
        next();
      });
    });
  });
  Schema.statics.findByCredentials = function(email, password) {
    let User = this;
    return User.findOne({
      email
    }).then(user => {
      if (!user) {
        return Promise.resolve(false);
      }
      return new Promise((resolve, reject) => {
        bcrypt.compare(password, user.password, (err, res) => {
          if (res) {
            resolve(user);
          } else {
            console.log("user not found");
            let message = "Loginerr2";
            reject(message);
          }
        });
      });
    });
  };
  //schema method to create reset password token.
  Schema.statics.createResetPasswordToken = function(email, host) {
    if (process.env.emailAuth) {
      let User = this;
      let buf = crypto.randomBytes(20);
      let token = buf.toString("hex");
      return User.findOne({
        email: email
      })
        .then(user => {
          if (!User) {
            return Promise.reject(`no User with email-id ${email} exists`);
          }
          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000;
          return user.save().then(() => {
            return user;
          });
        })
        .then(user => {
          let userType = "User";
          if (user.admin_id) {
            userType = "Admin";
          }
          let smtpTransport = nodemailer.createTransport({
            service: process.env.emailService,
            auth: {
              user: process.env.emailUser,
              pass: process.env.emailPass
            }
          });
          let mailOptions = {
            to: user.email,
            from: "passwordreset@demo.com",
            subject: "Password reset",
            text:
              "Password Reset Mail \n\n" +
              "Click on the below link to reset your password\n\n" +
              "http://" +
              host +
              "/reset-password/" +
              token +
              "/" +
              userType +
              "\n\n"
          };
          return smtpTransport.sendMail(mailOptions).then(() => {
            return {
              message: "email sent."
            };
          });
        })
        .catch(e => {
          console.log("error in redet password", e);
        });
    } else {
      return "email service has been disabled. so password reset cannot be used.";
    }
  };

  //schema statics to change the password
  Schema.statics.changePassword = function(token, newPassword) {
    if (process.env.emailAuth) {
      let User = this;
      return User.findOne({
        resetPasswordToken: token,
        resetPasswordExpires: {
          $gt: Date.now()
        }
      })
        .then(user => {
          if (!user) {
            return Promise.reject(
              "password reset token is invalid or has expired."
            );
          }
          user.password = newPassword;
          user.resetPasswordToken = undefined;
          user.resetPasswordExpires = undefined;

          return user.save().then(() => {
            return user;
          });
        })
        .then(user => {
          let smtpTransport = nodemailer.createTransport({
            service: process.env.emailService,
            auth: {
              user: process.env.emailUser,
              pass: process.env.emailPass
            }
          });
          let mailOptions = {
            to: user.email,
            from: "passwordreset@demo.com",
            subject: "Your Password has been changed",
            text: "Your password has been changed successfully"
          };
          return smtpTransport.sendMail(mailOptions).then(() => {
            return { message: "password successfully changed." };
          });
        });
    } else {
      return "email service has been disabled. so password reset cannot be used.";
    }
  };
  if (add) {
    Schema.add(add);
  }
  return Schema;
};
let userSchema = new UserSchema({
  tests: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UserTest"
    }
  ],
  currentAverage: Number
});

userSchema.statics.findByIdAndSaveTest = function(id, test) {
  let User = this;
  return User.findById(id)
    .then(user => {
      user.tests.push(test.id);
      if (user.currentAverage) {
        user.currentAverage =
          (user.currentAverage * (user.tests.length - 1) + test.score) /
          user.tests.length;
      } else {
        user.currentAverage = test.score;
      }

      return user.save().then(user => {
        console.log(user);
        return user;
      });
    })
    .catch(e => {
      console.log("logging error in find by id and save", e);
    });
};

let adminSchema = new UserSchema({
  admin_id: {
    type: Number,
    required: true,
    unique: true
  }
});
let Admin = mongoose.model("Admin", adminSchema);
let User = mongoose.model("User", userSchema);
module.exports = {
  User: User,
  Admin: Admin
};
