const mongoose = require("mongoose");

let questionSchema = new mongoose.Schema({
  description: {
    type: String,
    required: true
  },
  answer: {
    type: String,
    required: true
  },
  options: Array
});

module.exports = mongoose.model("Question", questionSchema);
