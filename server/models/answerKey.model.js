const mongoose = require("mongoose");

let answerKeySchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  question: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Question"
  },
  answer: { type: String },
  time: Number,
  score: Number,
  submitted: { type: Boolean, default: false }
});
answerKeySchema.post("save", function() {
  let response = this;
  response.populate("question", (err, answer) => {
    if (answer.answer === answer.question.answer) {
      response.update({ $set: { score: 1 } }, (err, updatedResponse) => {
        return;
      });
    } else {
      response.update({ $set: { score: 0 } }, (err, updatedResponse) => {
        return;
      });
    }
  });
});
module.exports = mongoose.model("AnswerKey", answerKeySchema);
