const mongoose = require("mongoose");

let UserTestSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  test: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Test"
  },
  submitTime: Number,
  answerKey: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AnswerKey"
    }
  ],
  attemptDate: Date,
  score: Number,
  userSubmitted: { type: Boolean, default: false }
});

UserTestSchema.pre("find", function() {
  this.populate("test");
});
module.exports = mongoose.model("UserTest", UserTestSchema);
