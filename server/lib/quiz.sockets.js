const events = require("events");
const testEvents = new events.EventEmitter();
const Test = require("../models/test.model");
const Question = require("../models/question.model");
const User = require("../models/user.model");
const UserTest = require("../models/user.test.model");
const AnswerKey = require("../models/answerKey.model");
const passportSetup = require("../config/passport.setup");

//define the controller for the real time test app
module.exports.controller = server => {
  let io = require("socket.io")(server);
  //use socket middleware to authenticate socket requests.
  io.use((socket, next) => {
    passportSetup.sessionMiddleware(socket.request, {}, next);
  });
  io.on("connection", socket => {
    let user = socket.request.session.passport.user; //get user data from session object
    let userAnswerKey = []; //initialize array to keep track of users answers.
    let userTestId = null; //declare user TestId variable
    let testtime = 0; //to keep track of time.
    let cleanEnd = false; //to track if the test was ended cleanly.

    socket.on("startTest", testId => {
      testEvents.emit("sendTest", testId); //emit event to receive the test details from the db.
      testEvents.once("testFound", test => {
        testEvents.emit("registerTest", {
          testId: testId,
          testName: test.testname,
          userId: user._id
        }); //once the test is found register that test into userTest db.
        testEvents.once("registeredNewTest", data => {
          //register new test is returned only if there is no un submitted test in userTest db with the requested testId.
          userTestId = data; //once new test is registered the test id is added to userTestId variable. to make further db calls to the userTest collection for adding answers.
          console.log("user test id in registered new test", userTestId);
          testEvents.emit("beginTest");
        });
        testEvents.once("beginTest", () => {
          socket.emit("sendTest", { test, userTestId }, () => {
            testtime = test.timelimit;
            let timeout = setInterval(() => {
              testtime -= 1;
              socket.emit("time", testtime);
              if (testtime === 0) {
                cleanEnd = true;
                clearInterval(timeout);
                socket.emit("testEnded", { userTestId });
                testEvents.emit("submitTest", {
                  userTestId: userTestId,
                  testTime: test.timelimit,
                  userId: user
                });
                socket.disconnect();
              }
            }, 1000);
            socket.once("endTestManual", (data, callback) => {
              cleanEnd = true;
              clearInterval(timeout);
              socket.emit("testEnded", { userTestId });
              testEvents.emit("submitTest", {
                userTestId: userTestId,
                testTime: test.timelimit - testtime,
                userId: user
              });
              socket.disconnect();
            });
          });
          let keyIndex = undefined;
          let lastAnswerTime = test.timelimit;
          socket.on("submitAnswer", (data, callback) => {
            if (testtime > 0) {
              data.time = lastAnswerTime - data.time;
              lastAnswerTime = testtime;
              keyIndex = userAnswerKey.findIndex(
                x => x.questionId == data.question_id
              );
              if (!keyIndex && keyIndex !== -1) {
                data.time = userAnswerKey[keyIndex].time + data.time;
                testEvents.emit("updateAnswer", {
                  updatedAnswer: data.answer,
                  time: data.time,
                  answerId: userAnswerKey[keyIndex].answerId
                });

                testEvents.once("updatedAnswer", data => {
                  let answerIndex = userAnswerKey.findIndex(
                    x => x._id === data._id
                  );
                  userAnswerKey[answerIndex] = data;
                });
                // userAnswerKey[keyIndex] = data;
                //fire update answer event to update the answer key in the db.
              } else {
                testEvents.emit("registerAnswer", {
                  user: user,
                  answer: data,
                  testId: userTestId
                });
                testEvents.once("registeredAnswer", data => {
                  userAnswerKey.push(data);
                });
              }

              callback(data.question_id);
            }
          });
          socket.on("resetAnswer", (data, callback) => {
            keyIndex = userAnswerKey.findIndex(x => x.questionId == data);
            console.log("reset answer received", data, keyIndex);

            if (keyIndex !== -1) {
              testEvents.emit("resetAnswer", {
                answerId: userAnswerKey[keyIndex].answerId,
                userTestId: userTestId
              });

              callback();
            }
          });
        });
      });
    });

    socket.on("disconnect", () => {
      console.log("the user has disconnected.");
      if (!cleanEnd) {
        console.log("test ended abruptly deleting userTest");
        testEvents.emit("deleteTest", { userTestId });
      }
    });
  });

  testEvents.on("sendTest", id => {
    Test.findById(id)
      .populate("questions", "subject description options")
      .then(test => {
        if (!test) {
          return console.log("test not found");
        }
        testEvents.emit("testFound", test);
      });
  });
  testEvents.on("deleteTest", data => {
    console.log(data);
    UserTest.findByIdAndRemove(data.userTestId)
      .then(test => {
        console.log("test ended abruptly so test removed", test);
        AnswerKey.find({ _id: { $in: test.answerKey } })
          .remove()
          .then(answers => {
            console.log("answers removed due to abrupt test ending", answers);
            return;
          });
      })
      .catch(e => {
        return console.log("could not remove test", e);
      });
  });
  testEvents.on("submitTest", data => {
    console.log("logging data in submit test", data);
    return UserTest.findOne(data.userTestId)
      .populate("answerKey")
      .then(userTest => {
        userTest.userSubmitted = true;
        userTest.submitTime = data.testTime;

        let score = 0;
        userTest.answerKey.forEach(answer => {
          if (answer.submitted) {
            score += answer.score;
          }
          answer = answer._id;
        });
        userTest.score = score;
        return userTest.save().then(userTest => {
          let addNewTest = {
            id: userTest._id,
            score: userTest.score
          };
          console.log("logging user test in submit test", userTest);
          return User.User.findByIdAndSaveTest(data.userId, addNewTest).then(
            user => {
              console.log(user);
              return;
            }
          );
        });
      })
      .catch(e => {
        return console.log("error in submitting the test.", e);
      });
  });
  testEvents.on("registerTest", data => {
    console.log("logging data in register test", data);
    let newUserTest = new UserTest({
      user: data.userId,
      test: data.testId,
      answerKey: [],
      attemptDate: new Date().getTime()
    });
    return newUserTest
      .save()
      .then(newTest => {
        return testEvents.emit("registeredNewTest", newTest._id);
      })
      .catch(err => {
        return console.log(
          "returning error from register test catch call",
          err
        );
      });
  });
  testEvents.on("registerAnswer", data => {
    let newAnswerKey = new AnswerKey({
      user: data.user,
      question: data.answer.question_id,
      answer: data.answer.answer,
      time: data.answer.time,
      submitted: true
    });
    newAnswerKey.save((err, newAnswer) => {
      if (err) {
        return console.log("answer key save error", err);
      }

      testEvents.emit("registeredAnswer", {
        answerId: newAnswer._id,
        questionId: newAnswer.question,
        time: newAnswer.time,
        submitted: newAnswer.submitted
      });
      UserTest.findByIdAndUpdate(
        data.testId,
        { $push: { answerKey: newAnswer._id } },
        (err, updatedTest) => {
          if (err) {
            console.log("error in adding the answer to the tests", err);
          }
        }
      );
      return console.log("returning success register answer", newAnswer._id);
    });
  });

  testEvents.on("updateAnswer", data => {
    return AnswerKey.findOne(data.answerId)
      .then(answer => {
        answer.answer = data.updatedAnswer;
        answer.time = data.time;
        answer.submitted = true;
        return answer.save().then(answer => {
          return testEvents.emit("updatedAnswer", answer);
        });
      })
      .catch(e => {
        console.log(
          "error in updating the answer in the update answer event",
          err
        );
      });
  });
  testEvents.on("resetAnswer", data => {
    AnswerKey.findByIdAndUpdate(
      data.answerId,
      { $set: { submitted: false } },
      { new: true },
      (err, updatedAnswer) => {
        console.log("updated Answer");
      }
    );
  });
};
